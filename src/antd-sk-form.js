
import { SkFormImpl }  from '../../sk-form/src/impl/sk-form-impl.js';

export class AntdSkForm extends SkFormImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'form';
    }

    beforeRendered() {
        super.beforeRendered();
        this.attachStyleByPath(this.skTheme.basePath + '/antd-theme.css', this.comp.querySelector('span[slot=fields]'));
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }


}
